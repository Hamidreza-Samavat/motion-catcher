function initialize(){
// stellt eine Verbindung zum Server her
	var socket = io.connect();
	
// Eventhandler f�r "click" definieren und setzen und Variablen definieren	
	var checkbox = document.getElementById("checkbox");
	var body = document.getElementById("body");
	var alarm = document.getElementById("alarm");
	checkbox.addEventListener("click", function(){
	// definition f�r checkbox 0 und 1 bei 1 bg-color Rot und Textausgabe bei 0 bg-color Wei� 
		if (this.checked){
			socket.emit("checkbox", {"value": 1});
			body.style["background-color"] = 'red';
			alarm.innerHTML = '++ALARM++'; 
			
		}
		else{
			socket.emit("checkbox", {"value": 0});
			body.style["background-color"] = 'white';
			alarm.innerHTML = ''; 
		}
	});
	
// Nachricht "checkbox" vom Server empfangen
	var checkbox = document.getElementById("checkbox");
	var body = document.getElementById("body");
	var alarm = document.getElementById("alarm");
	socket.on("checkbox", function(data){
		// definition f�r checkbox 0 und 1 bei 1 bg-color Rot und Textausgabe bei 0 bg-color Wei�
		if (data.value == 1){
			checkbox.checked = true;
			body.style["background-color"] = 'red';
			alarm.innerHTML = '++ALARM++'; 
}
		else{
			checkbox.checked = false;
			body.style["background-color"] = 'white';
			alarm.innerHTML = '';
		}
	});
}