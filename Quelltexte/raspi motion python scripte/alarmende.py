#!/usr/bin/python

#socket.io importieren
from socketIO_client import SocketIO

#Verbindung zum Node Server herstellen
socketIO = SocketIO('192.168.1.2', 3000)


#libs importieren
import time
import datetime

#Ausgabe für keine Bewegung
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print(" ")
print(" ")
print(" ")
print("Warte auf Bewegung...")
print(" ")
print(" ")
print(" ")
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")



#Nachricht an Server checkbox 0
socketIO.emit('checkbox', {'value': '0'})

