#!/usr/bin/python


#socket.io importieren
from socketIO_client import SocketIO

#Verbindung zum Node Server herstellen
socketIO = SocketIO('192.168.1.2', 3000)

#libs importieren
import time
import datetime
import pygame

#Ausgabe bei Erkennung
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print(" ")
print(" ")
print(" ")
print("ALARM %s: Bewegung erkannt!" % datetime.datetime.now())
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")

#Nachricht an Server checkbox 1
socketIO.emit('checkbox', {'value': '1'})


#Soundausgabe über pygame 
pygame.mixer.init()
pygame.mixer.music.load("haha.wav")
pygame.mixer.music.play()
while pygame.mixer.music.get_busy() == True:
    continue

